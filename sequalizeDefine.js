
// We need to declare an interface for our model that is basically what our class would be
import {  Model} from "sequelize";
interface MyModel extends Model {
    readonly id: number;
  }
  
  // Need to declare the static model so `findOne` etc. use correct types.
  type MyModelStatic = typeof Model & {
    new (values?: object, options?: BuildOptions): MyModel;
  }
  
  // TS can't derive a proper class definition from a `.define` call, therefor we need to cast here.
  const MyDefineModel = <MyModelStatic>sequelize.define('MyDefineModel', {
    id: {
      primaryKey: true,
      type: DataTypes.INTEGER.UNSIGNED,
    }
  });
  
  function stuffTwo() {
    MyDefineModel.findByPk(1, {
      rejectOnEmpty: true,
    })
    .then(myModel => {
      console.log(myModel.id);
    });