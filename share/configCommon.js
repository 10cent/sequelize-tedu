//@ts-check
const config = require('config')
class ConfigCommon {
    configDb() {
        switch (config.get('MODE_BUILD')) {
            case 1:
                return config.get('dbDev')
            case 2:
                return config.get('dbUat')
            case 3:
                return config.get('dbTraining')
        }
        return config.get('dbDev')
    }
}