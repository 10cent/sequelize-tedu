'use strict';

const  Sequelize = require('sequelize')



class User extends Model { }
User.init({
  // attributes
  firstName: {
    type:  STRING,
    allowNull: false
  },
  lastName: {
    type:  STRING
    // allowNull defaults to true
  }
}, {
    sequelize,
    modelName: 'user'
    // options
  });